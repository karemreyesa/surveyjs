import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

import { IniciarSesionPage } from '../pages/iniciar-sesion/iniciar-sesion';

import { TabsAdminPage } from '../pages/tabs-admin/tabs-admin';
import { ResultadosPage } from '../pages/resultados/resultados';
import { CerrarSesionPage } from '../pages/cerrar-sesion/cerrar-sesion';
import { RegisterPage } from '../pages/register/register';


import { SurveyDetailsPage } from '../pages/survey-details/survey-details';
import { SurveyResultsPage } from '../pages/survey-results/survey-results';

import { SurveyComponent } from '../components/survey/survey';
import { SurveyProvider } from '../providers/survey/survey';
import { ApiWrapper } from '../providers/survey/api-wrapper';
import { ChartComponent } from '../components/chart/chart';

import { ChartsModalPage } from '../modals/charts-modal';

import { TimingInterceptor } from '../interceptors/timing-interceptor';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthProvider } from '../providers/auth/auth';
import { FIREBASE_CONFIG } from './app.firebase.config';


@NgModule({
    declarations: [
        MyApp,
        HomePage,
        IniciarSesionPage,
        SurveyComponent,
        SurveyDetailsPage,
        SurveyResultsPage,
        ChartComponent,
        ChartsModalPage,
        LoginPage,
        ResultadosPage, 
        TabsAdminPage,
        CerrarSesionPage, RegisterPage
        
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(MyApp),
        AngularFireModule.initializeApp(FIREBASE_CONFIG),
        AngularFireDatabaseModule,
        AngularFireAuthModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        IniciarSesionPage,
        SurveyDetailsPage,
        SurveyResultsPage,
        ChartsModalPage,
        LoginPage,
        ResultadosPage,
        TabsAdminPage,
        CerrarSesionPage, RegisterPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        {provide: HTTP_INTERCEPTORS, useClass: TimingInterceptor, multi: true},
        SurveyProvider,
        ApiWrapper,
    AuthProvider
    ]
})
export class AppModule {}
