import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ResultadosPage } from '../resultados/resultados';
import { CerrarSesionPage } from '../cerrar-sesion/cerrar-sesion';
import { RegisterPage } from '../register/register';


@Component({
  selector: 'page-tabs-admin',
  templateUrl: 'tabs-admin.html',
})
export class TabsAdminPage {


  tab1Root = ResultadosPage;
  tab2Root = RegisterPage;
  tab3Root = CerrarSesionPage;
  

  constructor(private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams) {
  }


  

}
