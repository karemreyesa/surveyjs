import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { User } from "../../models/user"
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from "../home/home"

import { ResultadosPage } from '../resultados/resultados';
import { TabsAdminPage } from '../tabs-admin/tabs-admin';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user = {} as User;

  constructor(private afAuth: AngularFireAuth,
    public navCtrl: NavController, 
    public navParams: NavParams,
    private alertCtrl: AlertController) {
  }

  async login(user: User){
    try{
      const result = this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
      if(result){
        this.navCtrl.setRoot(TabsAdminPage);
      }
    } catch(e){
      this.ErrorAlert(e);
    }
  }

  register(){
    this.navCtrl.push('RegisterPage');
  }
  ErrorAlert(e) {
    let alert = this.alertCtrl.create({
      title: e,
      buttons: ['Aceptar']
    });
    alert.present();
  }

}
