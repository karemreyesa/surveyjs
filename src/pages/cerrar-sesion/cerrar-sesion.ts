import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-cerrar-sesion',
  templateUrl: 'cerrar-sesion.html',
})
export class CerrarSesionPage {

  constructor(private app: App,public afA: AngularFireAuth,public auth: AuthProvider,private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams) {
  }
  
  qCerrar(){
    
      this.afA.auth.signOut().then(() => {
        this.app.getRootNav().setRoot(HomePage);
     
      });
    
    
  }

  qAlert() {
    let alert = this.alertCtrl.create({
      title: "¿Seguro que desea Cerrar Sesión?",
      buttons: ['Aceptar']
    });
    alert.present();
  }

 
  

}
